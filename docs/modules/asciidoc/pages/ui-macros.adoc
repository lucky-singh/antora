= User Interface Macros
ifndef::env-site,env-github[]
include::_attributes.adoc[]
endif::[]
// Settings
:idprefix:
:idseparator: -
:experimental:
:example-caption!:
:linkattrs:
// External URIs
:uri-adoc-manual: http://asciidoctor.org/docs/user-manual
:uri-button: {uri-adoc-manual}/#ui-buttons
:uri-keyboard: {uri-adoc-manual}/#keyboard-shortcuts
:uri-menu: {uri-adoc-manual}/#menu-selections

On this page, you'll learn:

* [x] How to mark up a button (press btn:[Submit]) with AsciiDoc.
* [x] How to mark up a keybindings (kbd:[Ctrl+Shift+N]) with AsciiDoc.
* [x] How to mark up a menu (menu:View[Zoom > Reset]) with AsciiDoc.

== Set the experimental attribute

The attribute `experimental` must be set in the xref:page-header.adoc#page-attrs[page header] or globally to enable the UI macros styles in HTML.

== Button syntax

Communicate that a user should press a button with the button macro.

.Button UI macro
[source,asciidoc]
----
Press the btn:[Submit] button when you are finished the survey.

Select a file in the file navigator and click btn:[Open].
----

.Result
====
Press the btn:[Submit] button when you are finished the survey.

Select a file in the file navigator and click btn:[Open].
====

[discrete]
==== Asciidoctor resources

* {uri-button}[UI buttons^]

== Keybinding syntax

Create keyboard shortcuts with the keybinding macro.

.Keybinding UI macro
[source,asciidoc]
----
Press kbd:[esc] to exit insert mode.

Use the shortcut kbd:[Ctrl+T] to open a new tab in your browser.

kbd:[Ctrl+Shift+N] will open a new incognito window.
----

.Result
====
Press kbd:[esc] to exit insert mode.

Use the shortcut kbd:[Ctrl+T] to open a new tab in your browser.

kbd:[Ctrl+Shift+N] will open a new incognito window.
====

[discrete]
==== Asciidoctor resources

* {uri-keyboard}[Keyboard shortcuts^]

== Menu syntax

Show readers how to select a menu item with the menu macro.

.Menu UI macro
[source,asciidoc]
----
To save the file, select menu:File[Save].

Select menu:View[Zoom > Reset] to reset the zoom level to the default setting.
----

.Result
====
To save the file, select menu:File[Save].

Select menu:View[Zoom > Reset] to reset the zoom level to the default setting.
====

[discrete]
==== Asciidoctor resources

* {uri-menu}[Menu selections^]
